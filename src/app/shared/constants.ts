export enum messageTypes {
  inbox = 'Inbox',
  newMessage = 'New Message'
};

export const typeItems = [
  {
    type: messageTypes.inbox,
    img: require('./images/page.png')
  },
  {
    type: messageTypes.newMessage,
    img: require('./images/newMessage.png')
  }
];
