import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MailInterface} from '../../models/Mail.interface';
import {map} from 'rxjs/operators';
import * as moment from 'moment';
import {Observable} from 'rxjs';
import {NewMessageInteface} from '../../models/NewMessage.inteface';
import {Subject} from 'rxjs/Subject';

@Injectable()
export class MailsService {
    replyMailStream = new Subject<MailInterface>();

    constructor(private httpClient: HttpClient) {
    }

    getMails(): Observable<MailInterface[]> {
        return this.httpClient.get<MailInterface[]>('inbox/messages')
            .pipe(map(messages => {
                return messages.map(message => {
                    message.created = moment(message.created).format('D MMM YYYY h:mm a');

                    return message;
                });
            }));
    }

    sendMessage(messageData: NewMessageInteface): Observable<any> {
        const body = new URLSearchParams();

        for (const key of Object.keys(messageData)) {
            body.set(key, messageData[key]);
        }

        const options = {
            headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
        };

        return this.httpClient.post('inbox/messages', body.toString(), options);
    }

    getRecipients(): Observable<string[]> {
        return this.httpClient.get<string[]>('inbox/recipients');
    }

    deleteMail(id: string): Observable<any> {
        return this.httpClient.delete(`inbox/messages/${id}`);
    }


}
