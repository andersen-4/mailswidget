import {Component, Input, OnInit} from '@angular/core';
import {MailsService} from '../shared/services/mails.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NewMessageInteface} from '../models/NewMessage.inteface';
import {MailInterface} from '../models/Mail.interface';

@Component({
    selector: 'app-new-mail',
    templateUrl: './new-mail.component.html',
    styleUrls: ['./new-mail.component.css']
})
export class NewMailComponent implements OnInit {
    @Input() newMessage: MailInterface;

    recipients: string[];
    form: FormGroup;
    messageData: NewMessageInteface;

    constructor(private messagesService: MailsService,
                private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        const replyMessage = this.newMessage
            ? `<reply "${this.newMessage.message}">`
            : '';

        this.newMessage = this.newMessage || <MailInterface>{};
        this.messagesService.getRecipients()
            .subscribe(recipients => {
                this.recipients = recipients;
            });

        this.form = this.formBuilder.group({
            recipient: [this.newMessage.sender || '', Validators.required],
            subject: [this.newMessage.subject, Validators.required],
            message: [replyMessage, Validators.required]
        });

        this.form.valueChanges.subscribe(
            messageData => {
                this.messageData = messageData;
            }
        );
    }

    sendMail() {
        this.messageData.sender = 'me';
        this.messagesService.sendMessage(this.messageData)
            .subscribe();
    }
}
