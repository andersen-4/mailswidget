import {Component, OnInit} from '@angular/core';
import {messageTypes, typeItems} from './shared/constants';
import {MailsService} from './shared/services/mails.service';
import {MailInterface} from './models/Mail.interface';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    title: string = messageTypes.inbox;
    messageTypes = messageTypes;
    mailImage = require('./shared/images/mail.png');
    typeItems = typeItems;
    newMessage: MailInterface;

    constructor(private mailsService: MailsService) {

    }

    ngOnInit() {
        this.mailsService.replyMailStream
            .subscribe(mail => {
                this.title = messageTypes.newMessage;
                this.newMessage = mail;
            });
    }

    setType(type: string) {
        this.newMessage = null;
        this.title = type;
    }
}
