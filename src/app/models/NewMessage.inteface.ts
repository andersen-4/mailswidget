export interface NewMessageInteface {
  recipients: string;
  subject: string;
  message: string;
  sender: string;
}
