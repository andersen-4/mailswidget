export interface MailInterface {
  id: string;
  created: string;
  subject: string;
  message: string;
  displayed: string;
  sender: string;
  recipient: string;
}
