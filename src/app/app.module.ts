import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {MessagesListComponent} from './mails-list/mails-list.component';
import {MailItemComponent} from './mails-list/mail-item/mail-item.component';
import { NewMailComponent } from './new-mail/new-mail.component';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import {AddBaseUrlInterceptor} from './shared/interceptors/add-base-url.interceptor';
import {AddTokenInterceptor} from './shared/interceptors/add-token.interceptor';
import {MailsService} from './shared/services/mails.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    MessagesListComponent,
    MailItemComponent,
    NewMailComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AddBaseUrlInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: AddTokenInterceptor, multi: true},
    MailsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
