import {Component, OnInit} from '@angular/core';
import {MailInterface} from '../models/Mail.interface';
import {MailsService} from '../shared/services/mails.service';
import {mergeMap} from 'rxjs/operators';

@Component({
    selector: 'app-mails-list',
    templateUrl: './mails-list.component.html',
    styleUrls: ['./mails-list.component.css']
})
export class MessagesListComponent implements OnInit {
    mails: MailInterface[];

    constructor(private mailsService: MailsService) {
    }

    ngOnInit() {
        this.mailsService.getMails()
            .subscribe(mails => {
                this.mails = mails;
            });
    }

    deleteMail(id: string) {
        this.mailsService.deleteMail(id)
            .pipe(
                mergeMap(() => this.mailsService.getMails())
            )
            .subscribe(mails => {
                this.mails = mails;
            });
    }
}
