import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MailInterface} from '../../models/Mail.interface';
import {MailsService} from '../../shared/services/mails.service';

@Component({
    selector: 'app-mail-item',
    templateUrl: './mail-item.component.html',
    styleUrls: ['./mail-item.component.css']
})
export class MailItemComponent implements OnInit {
    @Input() mail: MailInterface;
    @Output() deleteMail = new EventEmitter<string>();

    selected: boolean = false;
    extended: boolean;
    mailService: MailsService;
    mouseIsDown: boolean;
    timer: any;

    constructor(mailsService: MailsService) {
        this.mailService = mailsService;
    }

    ngOnInit() {
    }

    toggleExtended() {
        if (!this.timer) {
            return;
        }

        if (this.selected) {
            this.selected = false;

            return;
        }

        this.extended = !this.extended;
    }

    selectMail() {
        this.timer = setTimeout(() => {
            this.selected = !this.selected;
            this.mouseIsDown = false;
            this.timer = null;
        }, 300);
    }

    mouseUp() {
        clearTimeout(this.timer);
        this.mouseIsDown = false;
    }

    onDeleteMail() {
        this.deleteMail.emit(this.mail.id);
    }

    replyMail() {
        this.mailService.replyMailStream.next(this.mail);
    }

}
